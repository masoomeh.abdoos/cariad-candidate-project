## Cariad Android Candidate Project
I created the project and now it's written in Kotlin and follows Clean Architecture

![Project Screenshots](https://gitlab.com/masoomeh.abdoos/cariad-candidate-project/-/raw/main/screens.jpg)

__Project stack in a nutshell__
* MVVM
* Kotlin
* Navigation Component
* Dagger2
* Retrofit
* Mockito
