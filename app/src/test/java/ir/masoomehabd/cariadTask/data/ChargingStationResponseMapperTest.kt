package ir.masoomehabd.cariadTask.data

import ir.masoomehabd.cariadTask.data.map.ChargingStationResponseMapper
import org.junit.Before
import org.junit.Test
import com.google.common.truth.Truth
import ir.masoomehabd.cariadTask.domain.DataCharging
import ir.masoomehabd.cariadTask.data.network.model.AddressInfo
import ir.masoomehabd.cariadTask.data.network.model.Country
import ir.masoomehabd.cariadTask.data.network.model.DataProvider
import ir.masoomehabd.cariadTask.data.network.model.UsageType

class ChargingStationResponseMapperTest {

    private lateinit var mapper: ChargingStationResponseMapper

    @Before
    fun setup() {
        mapper = ChargingStationResponseMapper()
    }

    @Test
    fun `Map network comment response to domain level comment model`() {

        val dataCharging = DataCharging(
            AddressInfo(1,"title","address1","address2",null,null
            ,"123456",21, Country(),12.23,23.34,null,null,null,null),
            DataProvider(),
            UsageType()
        )

        val hashMapCharging = mapper.map(dataCharging)
        Truth.assertThat(hashMapCharging.addressInfo?.latitude).isEqualTo(dataCharging.addressInfo?.latitude)
        Truth.assertThat(hashMapCharging.addressInfo?.longitude).isEqualTo(dataCharging.addressInfo?.longitude)
        Truth.assertThat(hashMapCharging.addressInfo).isEqualTo(dataCharging.addressInfo)
        Truth.assertThat(hashMapCharging.dataProvider).isEqualTo(dataCharging.dataProvider)
        Truth.assertThat(hashMapCharging.usageType).isEqualTo(dataCharging.usageType)
        Truth.assertThat(hashMapCharging.addressInfo?.addressLine1).isEqualTo(dataCharging.addressInfo?.addressLine1)

    }

}