package ir.masoomehabd.cariadTask.api

import ir.masoomehabd.cariadTask.data.network.model.ChargingStation
import ir.masoomehabd.cariadTask.data.network.api.CarIadServiceApi

class FakeCarIadServiceApi : CarIadServiceApi {

    override suspend fun getChargingStations(
        key: String,
        distance: Int,
        latitude: Double,
        longitude: Double
    ): List<ChargingStation> {
        return listOf()
    }
}