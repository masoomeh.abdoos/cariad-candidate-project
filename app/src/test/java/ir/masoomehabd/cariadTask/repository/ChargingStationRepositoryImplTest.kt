package ir.masoomehabd.cariadTask.repository

import com.google.common.truth.Truth
import ir.masoomehabd.cariadTask.MainCoroutineRule
import ir.masoomehabd.cariadTask.data.network.api.CarIadServiceApi
import ir.masoomehabd.cariadTask.api.FakeCarIadServiceApi
import ir.masoomehabd.cariadTask.data.map.ChargingStationResponseMapper
import ir.masoomehabd.cariadTask.domain.ChargingDetail
import ir.masoomehabd.cariadTask.domain.DataCharging
import ir.masoomehabd.cariadTask.data.network.model.Response
import ir.masoomehabd.cariadTask.data.repository.ChargingStationRepositoryImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

const val key = ""

class ChargingStationRepositoryImplTest {

    private lateinit var fakeSuccessfulChargingStationRepository: ChargingStationRepositoryImpl
    private lateinit var failedChargingStationRepository: ChargingStationRepositoryImpl

    private lateinit var fakeSuccessfulCardIdServiceApi: FakeCarIadServiceApi
    @Mock private lateinit var failedCardIdServiceApi: CarIadServiceApi
    @Mock private lateinit var mockChargingStationMapper: ChargingStationResponseMapper
    @Mock private lateinit var mockDataCharging: DataCharging
    @Mock private lateinit var mockChargingStation: ChargingDetail


    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()


    @Before
    fun setup() {

        MockitoAnnotations.openMocks(this)
        fakeSuccessfulCardIdServiceApi = FakeCarIadServiceApi()
        fakeSuccessfulChargingStationRepository = ChargingStationRepositoryImpl(fakeSuccessfulCardIdServiceApi, mockChargingStationMapper)
        failedChargingStationRepository = ChargingStationRepositoryImpl(failedCardIdServiceApi, mockChargingStationMapper)
        Mockito.`when`(mockChargingStationMapper.map(mockDataCharging)).thenReturn(mockChargingStation)
    }

    @Test
    fun `return successful response`() {

        runBlocking {
            val chargingResponse = fakeSuccessfulChargingStationRepository.getChargingStation(key)
            Truth.assertThat(chargingResponse is Response.Success).isTrue()
        }
    }

    @Test
    fun `return failed response`() {

        runBlocking {
            Mockito.`when`(failedCardIdServiceApi.getChargingStations(key)).thenThrow(RuntimeException())
            val chargingResponse = failedChargingStationRepository.getChargingStation(key)
            Truth.assertThat(chargingResponse is Response.Error).isTrue()
        }
    }
}