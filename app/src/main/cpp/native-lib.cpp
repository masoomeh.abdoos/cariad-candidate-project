//
// Created by masoomehabdoos on 18/12/21.
//
#include <jni.h>
#include <string>

std::string key = "1e2cb9c6-a0e9-4a68-bc09-f3c97a6bd8e4";


extern "C"
JNIEXPORT jstring JNICALL
Java_ir_masoomehabd_cariadTask_data_network_BaseKey_getKeyFromJNI(JNIEnv *env, jclass clazz) {
    return env->NewStringUTF(key.c_str());
}