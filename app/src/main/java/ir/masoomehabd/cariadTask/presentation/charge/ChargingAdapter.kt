package ir.masoomehabd.cariadTask.presentation.charge

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import ir.masoomehabd.cariadTask.databinding.ItemChargingBinding
import ir.masoomehabd.cariadTask.domain.ChargingDetail
import java.util.Random


class ChargingAdapter() : RecyclerView.Adapter<ChargingAdapter.ChargingViewHolder>() {

    var data: List<ChargingDetail> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChargingViewHolder {
        return ChargingViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ChargingViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun updateData(data: List<ChargingDetail>) {
        this@ChargingAdapter.data = data
        notifyDataSetChanged()
    }

    class ChargingViewHolder(private val binding: ItemChargingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(charging: ChargingDetail) {
            binding.chargeBody.text = charging.dataProvider.license
            binding.chargeTitle.text =
                " ${charging.addressInfo?.country?.title}" +
                "\n ${charging.addressInfo?.title} "

            binding.chargeThumbnail
                .load(charging.dataProvider.websiteURL){

                    this.error(ColorDrawable(getRandomColor()))
                }

            binding.root.setOnClickListener {

                val action =
                    ChargingLstFragmentDirections.actionChargeLstFragmentToDetailChargeStationFragment(
                        charging
                    )
                binding.root.findNavController().navigate(action)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ChargingViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemChargingBinding.inflate(layoutInflater, parent, false)
                return ChargingViewHolder(binding)
            }
        }

        private fun getRandomColor(): Int {
            val random = Random()
            return -0x1000000 or random.nextInt(0xffffff)
        }

    }
}