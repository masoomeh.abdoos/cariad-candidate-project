package ir.masoomehabd.cariadTask.presentation.station

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import dagger.android.support.DaggerFragment
import ir.masoomehabd.cariadTask.databinding.DetailChargingStationBinding

class DetailChargingStationFragment : Fragment() {

    private val TAG = "DetailChargingStationFragment"
    private  var _binding : DetailChargingStationBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<DetailChargingStationFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        _binding = DetailChargingStationBinding.inflate(inflater,container,false)
        binding.chargingItem = args.currentChargingStation
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}