package ir.masoomehabd.cariadTask.presentation.charge

import android.util.Log
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.masoomehabd.cariadTask.data.network.BaseKey.Companion.getKeyFromJNI
import ir.masoomehabd.cariadTask.data.network.model.Response
import ir.masoomehabd.cariadTask.data.repository.ChargingStationRepositoryImpl
import ir.masoomehabd.cariadTask.domain.ChargingDetail
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChargingStationsViewModel  @Inject constructor(private val repository: ChargingStationRepositoryImpl)
    : LifecycleObserver,ViewModel()
{


     var chargingStations: MutableLiveData<Response<List<ChargingDetail>>> = MutableLiveData()
    init {
        requestCharge(getKeyFromJNI())
    }

      fun requestCharge(key: String)  = viewModelScope.launch  {
          chargingStations.value = repository.getChargingStation(key)

    }
}