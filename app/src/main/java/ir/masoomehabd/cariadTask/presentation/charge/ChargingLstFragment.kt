package ir.masoomehabd.cariadTask.presentation.charge

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.DaggerFragment
import ir.masoomehabd.cariadTask.R
import ir.masoomehabd.cariadTask.data.network.BaseKey.Companion.getKeyFromJNI
import ir.masoomehabd.cariadTask.data.network.model.Response
import ir.masoomehabd.cariadTask.databinding.ActivityChargingListBinding
import ir.masoomehabd.cariadTask.presentation.dialog.NetworkErrorDialog
import javax.inject.Inject

class ChargingLstFragment : DaggerFragment() {

    lateinit var chargingStationsViewModel: ChargingStationsViewModel
        @Inject set
    private val adapter: ChargingAdapter by lazy { ChargingAdapter() }

    private val TAG = "ChargingLstFragment"
    private  var _binding : ActivityChargingListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        _binding = ActivityChargingListBinding.inflate(inflater,container,false)

        setHasOptionsMenu(true)
        setupRecyclerView()

        val networkErrorDialog = NetworkErrorDialog(
            requireContext()
        ) {
            chargingStationsViewModel.requestCharge(getKeyFromJNI())
        }

        chargingStationsViewModel.chargingStations.observe(viewLifecycleOwner) { response ->
            binding.progressBar.visibility = View.GONE

            when (response) {
                is Response.Success -> {
                    response.data?.let {
                        adapter.updateData(it)
                    }
                    if (networkErrorDialog.isShowing) {
                        networkErrorDialog.dismiss()
                    }
                }

                is Response.Error -> {
                    if (networkErrorDialog.isShowing) {
                        networkErrorDialog.showIdleState()
                    } else {
                        networkErrorDialog.show()
                    }
                }
            }
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.charge_fragment_menu, menu)
    }

    private fun setupRecyclerView() {
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}