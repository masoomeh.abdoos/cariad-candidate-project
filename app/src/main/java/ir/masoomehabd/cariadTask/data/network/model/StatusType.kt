package ir.masoomehabd.cariadTask.data.network.model

import com.google.gson.annotations.SerializedName


data class StatusType (

  @SerializedName("IsOperational"    ) var isOperational    : Boolean? = null,
  @SerializedName("IsUserSelectable" ) var isUserSelectable : Boolean? = null,
  @SerializedName("ID"               ) var id               : Int?     = null,
  @SerializedName("Title"            ) var title            : String?  = null

)