package ir.masoomehabd.cariadTask.data.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.masoomehabd.cariadTask.presentation.charge.ChargingLstFragment
import ir.masoomehabd.cariadTask.presentation.station.DetailChargingStationFragment


@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    internal abstract fun contributeChargingLstFragment(): ChargingLstFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDetailChargingStationFragment(): DetailChargingStationFragment

}