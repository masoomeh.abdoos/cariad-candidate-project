package ir.masoomehabd.cariadTask.data.network.api

import ir.masoomehabd.cariadTask.data.network.model.ChargingStation
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CarIadServiceApi {

    @GET("poi")
    suspend fun getChargingStations(@Header("x-api-key") key: String,
                                    @Query("distance") distance: Int = 5,
                                    @Query("latitude") latitude: Double = 52.526,
                                    @Query("longitude") longitude: Double = 13.415,
//    @Query("position") position: Position = Position(52.526,13.415)

    ): List<ChargingStation>

}