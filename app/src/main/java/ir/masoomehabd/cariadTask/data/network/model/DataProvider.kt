package ir.masoomehabd.cariadTask.data.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataProvider (

    @SerializedName("WebsiteURL"             ) var websiteURL             : String?                 = null,
    @SerializedName("Comments"               ) var comments               : String?                 = null,
    @SerializedName("DataProviderStatusType" ) var dataProviderStatusType : DataProviderStatusType? = DataProviderStatusType(),
    @SerializedName("IsRestrictedEdit"       ) var isRestrictedEdit       : Boolean?                = null,
    @SerializedName("IsOpenDataLicensed"     ) var isOpenDataLicensed     : Boolean?                = null,
    @SerializedName("IsApprovedImport"       ) var isApprovedImport       : Boolean?                = null,
    @SerializedName("License"                ) var license                : String?                 = null,
    @SerializedName("DateLastImported"       ) var dateLastImported       : String?                 = null,
    @SerializedName("ID"                     ) var id                     : Int?                    = null,
    @SerializedName("Title"                  ) var title                  : String?                 = null

) : Parcelable