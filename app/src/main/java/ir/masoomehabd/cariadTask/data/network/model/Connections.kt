package ir.masoomehabd.cariadTask.data.network.model

import com.google.gson.annotations.SerializedName


data class Connections (

    @SerializedName("ID"               ) var id               : Int?            = null,
    @SerializedName("ConnectionTypeID" ) var connectionTypeID : Int?            = null,
    @SerializedName("ConnectionType"   ) var connectionType   : ConnectionType? = ConnectionType(),
    @SerializedName("Reference"        ) var reference        : String?         = null,
    @SerializedName("StatusTypeID"     ) var statusTypeID     : Int?            = null,
    @SerializedName("StatusType"       ) var statusType       : StatusType?     = StatusType(),
    @SerializedName("LevelID"          ) var levelID          : Int?            = null,
    @SerializedName("Level"            ) var level            : Level?          = Level(),
    @SerializedName("Amps"             ) var amps             : String?         = null,
    @SerializedName("Voltage"          ) var voltage          : String?         = null,
    @SerializedName("PowerKW"          ) var powerKW          : Double?         = null,
    @SerializedName("CurrentTypeID"    ) var currentTypeID    : Int?            = null,
    @SerializedName("CurrentType"      ) var currentType      : CurrentType?    = CurrentType(),
    @SerializedName("Quantity"         ) var quantity         : Int?            = null,
    @SerializedName("Comments"         ) var comments         : String?         = null

)