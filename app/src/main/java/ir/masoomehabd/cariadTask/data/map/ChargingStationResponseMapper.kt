package ir.masoomehabd.cariadTask.data.map

import ir.masoomehabd.cariadTask.domain.ChargingDetail
import ir.masoomehabd.cariadTask.domain.DataCharging
import javax.inject.Inject

class ChargingStationResponseMapper @Inject constructor() : Mapper<DataCharging, ChargingDetail> {

    override fun map(input: DataCharging): ChargingDetail =

        ChargingDetail(input.addressInfo,input.dataProvider,input.usageType)
}