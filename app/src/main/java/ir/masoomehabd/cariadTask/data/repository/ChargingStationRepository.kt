package ir.masoomehabd.cariadTask.data.repository

import ir.masoomehabd.cariadTask.domain.ChargingDetail
import ir.masoomehabd.cariadTask.data.network.model.Response

interface ChargingStationRepository {

    suspend fun getChargingStation(key:String) : Response<List<ChargingDetail>>
}