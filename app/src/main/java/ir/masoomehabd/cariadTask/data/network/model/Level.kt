package ir.masoomehabd.cariadTask.data.network.model

import com.google.gson.annotations.SerializedName


data class Level (

  @SerializedName("Comments"            ) var comments            : String?  = null,
  @SerializedName("IsFastChargeCapable" ) var isFastChargeCapable : Boolean? = null,
  @SerializedName("ID"                  ) var id                  : Int?     = null,
  @SerializedName("Title"               ) var title               : String?  = null

)