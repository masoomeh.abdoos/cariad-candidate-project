package ir.masoomehabd.cariadTask.data.di.module

import dagger.Module
import dagger.Provides
//import ir.masoomehabd.cariadTask.config.TokenManager
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


@Module
class NetModule() {

    lateinit var retrofitInstance: Retrofit
    var BASE_URL = "https://api.openchargemap.io/v3/"
    @Provides
    fun provideRetrofitInstance(): Retrofit {

        val httpClient: OkHttpClient.Builder = createHttpClient()
        retrofitInstance = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build()

        return retrofitInstance

    }

    private fun createHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
    }

}