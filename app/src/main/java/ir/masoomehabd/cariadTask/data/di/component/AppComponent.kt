package ir.masoomehabd.cariadTask.data.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ir.masoomehabd.cariadTask.Application
import ir.masoomehabd.cariadTask.data.di.module.ActivityBuilderModule
//import ir.masoomehabd.cariadTask.data.di.module.ActivityBuilderModule
import ir.masoomehabd.cariadTask.data.di.module.ApiModule
import ir.masoomehabd.cariadTask.data.di.module.ChargingModule
import ir.masoomehabd.cariadTask.data.di.module.FragmentsModule
//import ir.masoomehabd.cariadTask.data.di.module.FragmentsModule
import ir.masoomehabd.cariadTask.data.di.module.NetModule


@Component( modules = [AndroidSupportInjectionModule::class, ActivityBuilderModule::class ,
    FragmentsModule::class , ApiModule::class , NetModule::class, ChargingModule::class])

interface AppComponent : AndroidInjector<Application> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun netModule(netModule: NetModule): Builder
        fun build(): AppComponent
    }
}