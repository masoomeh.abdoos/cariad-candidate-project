package ir.masoomehabd.cariadTask.data.di.module

import dagger.Module
import dagger.Provides
import ir.masoomehabd.cariadTask.data.map.ChargingStationResponseMapper
import ir.masoomehabd.cariadTask.data.map.Mapper
import ir.masoomehabd.cariadTask.domain.ChargingDetail
import ir.masoomehabd.cariadTask.domain.DataCharging
import ir.masoomehabd.cariadTask.data.repository.ChargingStationRepository
import ir.masoomehabd.cariadTask.data.repository.ChargingStationRepositoryImpl

@Module
class ChargingModule {

    @Provides
    fun chargingMapper(mapper: ChargingStationResponseMapper):
            Mapper<DataCharging, ChargingDetail>{

       return mapper
    }

    @Provides
    fun repository(repository: ChargingStationRepositoryImpl): ChargingStationRepository {
        return repository
    }

}
