package ir.masoomehabd.cariadTask.data.network.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.Parceler

@Parcelize
data class ChargingStation (

    @SerializedName("DataProvider"           ) var dataProvider           : DataProvider?          = DataProvider(),
    @SerializedName("OperatorInfo"           ) var operatorInfo           : OperatorInfo?          = OperatorInfo(),
    @SerializedName("UsageType"              ) var usageType              : UsageType?             = UsageType(),
    @SerializedName("StatusType"             ) var statusType             : StatusType?            = StatusType(),
    @SerializedName("SubmissionStatus"       ) var submissionStatus       : SubmissionStatus?      = SubmissionStatus(),
//    @SerializedName("UserComments"           ) var UserComments           : String?                = null,
    @SerializedName("PercentageSimilarity"   ) var percentageSimilarity   : String?                = null,
//    @SerializedName("MediaItems"             ) var MediaItems             : String?                = null,
    @SerializedName("IsRecentlyVerified"     ) var isRecentlyVerified     : Boolean?               = null,
    @SerializedName("DateLastVerified"       ) var dateLastVerified       : String?                = null,
    @SerializedName("ID"                     ) var id                     : Int?                   = null,
    @SerializedName("UUID"                   ) var uuid                   : String?                = null,
    @SerializedName("ParentChargePointID"    ) var parentChargePointID    : String?                = null,
    @SerializedName("DataProviderID"         ) var dataProviderID         : Int?                   = null,
    @SerializedName("DataProvidersReference" ) var dataProvidersReference : String?                = null,
    @SerializedName("OperatorID"             ) var operatorID             : Int?                   = null,
    @SerializedName("OperatorsReference"     ) var operatorsReference     : String?                = null,
    @SerializedName("UsageTypeID"            ) var usageTypeID            : Int?                   = null,
    @SerializedName("UsageCost"              ) var usageCost              : String?                = null,
    @SerializedName("AddressInfo"            ) var addressInfo            : AddressInfo?           = AddressInfo(),
    @SerializedName("Connections"            ) var connections            : ArrayList<Connections> = arrayListOf(),
    @SerializedName("NumberOfPoints"         ) var numberOfPoints         : Int?                   = null,
    @SerializedName("GeneralComments"        ) var generalComments        : String?                = null,
    @SerializedName("DatePlanned"            ) var datePlanned            : String?                = null,
    @SerializedName("DateLastConfirmed"      ) var dateLastConfirmed      : String?                = null,
    @SerializedName("StatusTypeID"           ) var statusTypeID           : Int?                   = null,
    @SerializedName("DateLastStatusUpdate"   ) var dateLastStatusUpdate   : String?                = null,
//    @SerializedName("MetadataValues"         ) var MetadataValues         : String?                = null,
    @SerializedName("DataQualityLevel"       ) var dataQualityLevel       : Int?                   = null,
    @SerializedName("DateCreated"            ) var dateCreated            : String?                = null,
    @SerializedName("SubmissionStatusTypeID" ) var submissionStatusTypeID : Int?                   = null

):Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("DataProvider"),
        TODO("OperatorInfo"),
        TODO("UsageType"),
        TODO("StatusType"),
        TODO("SubmissionStatus"),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        TODO("AddressInfo"),
        TODO("Connections"),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
//        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    companion object : Parceler<ChargingStation> {

        override fun ChargingStation.write(parcel: Parcel, flags: Int) {
            parcel.writeString(percentageSimilarity)
            parcel.writeValue(isRecentlyVerified)
            parcel.writeString(dateLastVerified)
            parcel.writeValue(id)
            parcel.writeString(uuid)
            parcel.writeString(parentChargePointID)
            parcel.writeValue(dataProviderID)
            parcel.writeString(dataProvidersReference)
            parcel.writeValue(operatorID)
            parcel.writeString(operatorsReference)
            parcel.writeValue(usageTypeID)
            parcel.writeString(usageCost)
            parcel.writeValue(numberOfPoints)
            parcel.writeString(generalComments)
            parcel.writeString(datePlanned)
            parcel.writeString(dateLastConfirmed)
            parcel.writeValue(statusTypeID)
            parcel.writeString(dateLastStatusUpdate)
//            parcel.writeString(MetadataValues)
            parcel.writeValue(dataQualityLevel)
            parcel.writeString(dateCreated)
            parcel.writeValue(submissionStatusTypeID)
        }

        override fun create(parcel: Parcel): ChargingStation {
            return ChargingStation(parcel)
        }
    }
}