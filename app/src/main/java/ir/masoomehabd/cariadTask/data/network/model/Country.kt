package ir.masoomehabd.cariadTask.data.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Country (

  @SerializedName("ISOCode"       ) var iSOCode       : String? = null,
  @SerializedName("ContinentCode" ) var continentCode : String? = null,
  @SerializedName("ID"            ) var id            : Int?    = null,
  @SerializedName("Title"         ) var title         : String? = null

):Parcelable