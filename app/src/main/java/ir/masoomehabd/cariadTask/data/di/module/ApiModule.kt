package ir.masoomehabd.cariadTask.data.di.module


import dagger.Module
import dagger.Provides
import ir.masoomehabd.cariadTask.data.network.api.CarIadServiceApi

import retrofit2.Retrofit

@Module
class ApiModule {

    @Provides
    fun provideApi(retroFit: Retrofit): CarIadServiceApi {
        return retroFit.create(CarIadServiceApi::class.java)
    }
}