package ir.masoomehabd.cariadTask.data.repository


import android.util.Log
import ir.masoomehabd.cariadTask.data.network.api.Call
import ir.masoomehabd.cariadTask.data.network.api.CarIadServiceApi
import ir.masoomehabd.cariadTask.data.map.Mapper
import ir.masoomehabd.cariadTask.domain.ChargingDetail
import ir.masoomehabd.cariadTask.domain.DataCharging
import ir.masoomehabd.cariadTask.data.network.model.ChargingStation
import javax.inject.Inject

class ChargingStationRepositoryImpl @Inject constructor(
    private val carIadServiceApi: CarIadServiceApi
    , private val chargingStationsDataMapper: Mapper<DataCharging, ChargingDetail>
) : ChargingStationRepository {

    override suspend fun getChargingStation(key:String) = Call.safeCall {

        val data = carIadServiceApi.getChargingStations(key)
        mapChargingStation( data)
    }

    private fun mapChargingStation(chargingStations: List<ChargingStation>): List<ChargingDetail> {

       return chargingStations.map {chargingitem ->
           chargingStationsDataMapper.map(DataCharging(chargingitem.addressInfo,
               chargingitem.dataProvider!!,chargingitem.usageType))

        }

    }
}