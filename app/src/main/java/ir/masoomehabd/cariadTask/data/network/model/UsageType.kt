package ir.masoomehabd.cariadTask.data.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UsageType (

  @SerializedName("IsPayAtLocation"      ) var isPayAtLocation      : Boolean? = null,
  @SerializedName("IsMembershipRequired" ) var isMembershipRequired : Boolean? = null,
  @SerializedName("IsAccessKeyRequired"  ) var isAccessKeyRequired  : Boolean? = null,
  @SerializedName("ID"                   ) var id                   : Int?     = null,
  @SerializedName("Title"                ) var title                : String?  = null

) : Parcelable