package ir.masoomehabd.cariadTask.data.network

class BaseKey {
    companion object {
        init {
            System.loadLibrary("native-lib")
        }

        @JvmStatic
        external fun getKeyFromJNI(): String

    }
}