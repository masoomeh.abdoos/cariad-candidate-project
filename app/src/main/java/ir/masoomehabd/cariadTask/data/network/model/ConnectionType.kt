package ir.masoomehabd.cariadTask.data.network.model

import com.google.gson.annotations.SerializedName


data class ConnectionType (

  @SerializedName("FormalName"     ) var formalName     : String?  = null,
  @SerializedName("IsDiscontinued" ) var isDiscontinued : Boolean? = null,
  @SerializedName("IsObsolete"     ) var isObsolete     : Boolean? = null,
  @SerializedName("ID"             ) var id             : Int?     = null,
  @SerializedName("Title"          ) var title          : String?  = null

)