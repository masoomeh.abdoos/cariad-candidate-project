package ir.masoomehabd.cariadTask.data.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class AddressInfo (

    @SerializedName("ID"                ) var id                : Int?     = null,
    @SerializedName("Title"             ) var title             : String?  = null,
    @SerializedName("AddressLine1"      ) var addressLine1      : String?  = null,
    @SerializedName("AddressLine2"      ) var addressLine2      : String?  = null,
    @SerializedName("Town"              ) var town              : String?  = null,
    @SerializedName("StateOrProvince"   ) var stateOrProvince   : String?  = null,
    @SerializedName("Postcode"          ) var postcode          : String?  = null,
    @SerializedName("CountryID"         ) var countryID         : Int?     = null,
    @SerializedName("Country"           ) var country           : Country? = Country(),
    @SerializedName("Latitude"          ) var latitude          : Double?  = null,
    @SerializedName("Longitude"         ) var longitude         : Double?  = null,
    @SerializedName("ContactTelephone1" ) var contactTelephone1 : String?  = null,
    @SerializedName("ContactTelephone2" ) var contactTelephone2 : String?  = null,
    @SerializedName("ContactEmail"      ) var contactEmail      : String?  = null,
    @SerializedName("AccessComments"    ) var accessComments    : String?  = null,
    @SerializedName("RelatedURL"        ) var relatedURL        : String?  = null,
    @SerializedName("Distance"          ) var distance          : String?  = null,
    @SerializedName("DistanceUnit"      ) var distanceUnit      : Int?     = null

):Parcelable