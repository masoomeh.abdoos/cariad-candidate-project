package ir.masoomehabd.cariadTask.data.map

interface Mapper<I, O> {
    fun map(input: I): O
}