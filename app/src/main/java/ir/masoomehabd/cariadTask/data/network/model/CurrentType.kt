package ir.masoomehabd.cariadTask.data.network.model

import com.google.gson.annotations.SerializedName


data class CurrentType (

  @SerializedName("Description" ) var description : String? = null,
  @SerializedName("ID"          ) var id          : Int?    = null,
  @SerializedName("Title"       ) var title       : String? = null

)