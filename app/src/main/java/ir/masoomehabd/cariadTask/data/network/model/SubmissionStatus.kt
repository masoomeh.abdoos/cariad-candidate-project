package ir.masoomehabd.cariadTask.data.network.model

import com.google.gson.annotations.SerializedName


data class SubmissionStatus (

  @SerializedName("IsLive" ) var isLive : Boolean? = null,
  @SerializedName("ID"     ) var id     : Int?     = null,
  @SerializedName("Title"  ) var title  : String?  = null

)