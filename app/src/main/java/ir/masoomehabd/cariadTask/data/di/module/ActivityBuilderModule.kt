package ir.masoomehabd.cariadTask.data.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.masoomehabd.cariadTask.presentation.MainActivity

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun contributeChargingLstActivity() : MainActivity


}