package ir.masoomehabd.cariadTask.data.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OperatorInfo (

  @SerializedName("WebsiteURL"            ) var websiteURL            : String?  = null,
  @SerializedName("Comments"              ) var comments              : String?  = null,
  @SerializedName("PhonePrimaryContact"   ) var phonePrimaryContact   : String?  = null,
  @SerializedName("PhoneSecondaryContact" ) var phoneSecondaryContact : String?  = null,
  @SerializedName("IsPrivateIndividual"   ) var isPrivateIndividual   : Boolean? = null,
  @SerializedName("AddressInfo"           ) var addressInfo           : String?  = null,
  @SerializedName("BookingURL"            ) var bookingURL            : String?  = null,
  @SerializedName("ContactEmail"          ) var contactEmail          : String?  = null,
  @SerializedName("FaultReportEmail"      ) var faultReportEmail      : String?  = null,
  @SerializedName("IsRestrictedEdit"      ) var isRestrictedEdit      : String?  = null,
  @SerializedName("ID"                    ) var id                    : Int?     = null,
  @SerializedName("Title"                 ) var title                 : String?  = null

) : Parcelable