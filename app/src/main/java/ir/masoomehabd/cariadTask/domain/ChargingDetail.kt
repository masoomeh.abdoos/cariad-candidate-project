package ir.masoomehabd.cariadTask.domain

import android.os.Parcelable
import ir.masoomehabd.cariadTask.data.network.model.AddressInfo
import ir.masoomehabd.cariadTask.data.network.model.DataProvider
import ir.masoomehabd.cariadTask.data.network.model.UsageType
import kotlinx.parcelize.Parcelize

@Parcelize
class ChargingDetail (
        var addressInfo : AddressInfo? = AddressInfo(),
        var dataProvider: DataProvider = DataProvider(),
        var usageType   : UsageType?   = UsageType()
        ):Parcelable {
        override fun toString(): String {
                return "ChargingDetail(addressInfo=$addressInfo, dataProvider=$dataProvider, usageType=$usageType)"
        }
}