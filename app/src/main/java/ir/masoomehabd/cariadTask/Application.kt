package ir.masoomehabd.cariadTask

import dagger.android.AndroidInjector
import ir.masoomehabd.cariadTask.data.di.component.DaggerAppComponent

class Application : dagger.android.DaggerApplication()
{
    override fun applicationInjector(): AndroidInjector<out dagger.android.DaggerApplication>
    {
        return DaggerAppComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
    }

}